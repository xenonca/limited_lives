# Limited Lives
Limits the times players can die. Exceeding the limit will cause them to turn into a ghost or ban them.
***
## Description
You can define how many times a player will be able to respawn after dying. If that limit is exceeded the player will either be banned or turn into a ghost, depending on the settings.

## Licence
Code: MIT by xenonca  
Media: CC-BY-SA 4.0 by xenonca
