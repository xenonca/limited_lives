local ms = minetest.get_mod_storage()
local dlimit = tonumber(minetest.settings:get("death_limit")) or 3
local adeath = minetest.settings:get("after_death") or "ghost"

minetest.register_privilege("lim_lives", {
	description = "Manage player deaths",
	give_to_singleplayer = false,
	give_to_admin = true,
})

minetest.register_chatcommand("deaths", {
	params = "[<player>]",
	description = "Check a player's deaths",
	func = function(name, param)
		local param = param:trim()
		if param == "" or nil then
			local deaths = ms:get_int(name)
			minetest.chat_send_player(name, "You have died " .. deaths .. " times!")
		else
			local privs = minetest.get_player_privs(name)
			if not privs.lim_lives then
				return false, "Insufficient privileges!"
			elseif not minetest.player_exists(param) then
				return false, "Player " .. minetest.colorize("#FFFF00", param) .. " does not exist!"
			else
				local deaths = ms:get_int(param)
				minetest.chat_send_player(name, "Player " .. minetest.colorize("#FFFF00", param) .. " has died " .. deaths .. " times!")
			end
		end
	end
})

minetest.register_chatcommand("reset_deaths", {
	params = "[<player>]",
	description = "Reset a player's deaths",
	privs = {lim_lives=true},
	func = function(name, param)
		if param == "" or nil then
			ms:set_int(name, 0)
			minetest.chat_send_player(name, "Your deaths have been reset!")
		elseif not minetest.player_exists(param) then
			return false, "Player " .. minetest.colorize("#FFFF00", param) .. " does not exist!"
		else
			ms:set_int(param, 0)
			minetest.chat_send_player(name, "Deaths of " .. minetest.colorize("#FFFF00", param) .. " have been reset!")
			if param then
				minetest.chat_send_player(param, "Your deaths have been reset! Rejoin to leave your ghost behind.")
			end
		end
	end
})

local function ghost(name)
	local privs = minetest.serialize(minetest.get_player_privs(name))
	if privs ~= {} then
		ms:set_string(name .. "_privs", privs)
	end
	minetest.set_player_privs(name, {})
	local player = minetest.get_player_by_name(name)
	local props = player:get_properties()
	props.pointable = false
	props.textures = {"lim_lives_character.png"}
	props.use_texture_alpha = true
	props.makes_footstep_sound = false
	props.nametag_color = "#00000000"
	show_on_minimap = false
	player:set_properties(props)
	player:set_armor_groups({immortal=100})
end

minetest.register_on_dieplayer(function(player, reason)
	local name = player:get_player_name()
	ms:set_int(name, ms:get_int(name) + 1)
	local deaths = ms:get_int(name)
	if deaths > dlimit then
		if adeath == "ghost" then
			minetest.chat_send_player(name, "You died too often! You're now only a ghost on this server.")
			ghost(name)
		elseif adeath == "ban" then
			minetest.kick_player(name, "You died too often, time to say goodbye forever.")
		end
	else
		minetest.chat_send_player(name, "Death " .. deaths .. " out of " .. dlimit ..".")
	end
end)

minetest.register_on_prejoinplayer(function(name, ip)
	local deaths = ms:get_int(name)
	if deaths > dlimit and adeath == "ban" then
		return "You've died too often on this server..."
	end
end)

minetest.register_on_joinplayer(function(player, last_login)
	local name = player:get_player_name()
	local deaths = ms:get_int(name)
	if deaths > dlimit and adeath == "ghost" then
		minetest.chat_send_player(name, "You've died too often and remain a ghost on this server.")
		minetest.after(0.3, function()
			ghost(name)
		end)
	else
		local privs = minetest.deserialize(ms:get_string(name .. "_privs"))
		if privs ~= nil then
			minetest.set_player_privs(name, privs)
		end
	end
end)