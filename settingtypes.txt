#Times a player can die before being banned or turning into a ghost.
death_limit (Death Limit) int 3 1 100

#Define what happens when a player exceeds the death limit. When set to "ghost" a player will stil be able to join the game but without interact. Selecting "ban" bans a player from the game.
after_death (After Death) enum 1 ghost,ban